<?php

declare(strict_types=1);

namespace Screpper\Command;

use Screpper\Repository\BibleRepository;
use Screpper\Service\BibleKeyWordService;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This command loops all entries in the `bible` table and extracts keywords by verse.
 */
class KeyWordCommand extends AbstractCommand
{
    /** @var \Screpper\Repository\BibleRepository */
    private $bibleRepository;

    /** @var \Screpper\Service\BibleKeyWordService */
    private $bibleKeyWordService;

    public function __construct(
        BibleRepository $bibleRepository,
        BibleKeyWordService $bibleKeyWordService
    ) {
        parent::__construct();

        $this->bibleRepository = $bibleRepository;
        $this->bibleKeyWordService = $bibleKeyWordService;
    }

    protected function configure()
    {
        $this
            ->setName('screpper:process:key-words')
            ->setDescription('Process key words by chapter.');
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $output->writeln("\n<fg=black;bg=cyan>screpper key word processor</fg=black;bg=cyan>");
        $this->writeEmpty();
        $output->writeln('Building...');
        $this->writeEmpty();

        $bibleChapters = $this->bibleRepository->findAll();

        $chapterCounter = count($bibleChapters);
        $output->writeln(sprintf('Processing %s book/chapter combinations', $chapterCounter));
        $progressBar = new ProgressBar($output, $chapterCounter);
        $progressBar->start();

        foreach ($bibleChapters as $bible) {
            $this->bibleKeyWordService->processKeyWordsByChapter($bible, $bible->getText()['keyWords']);
            $progressBar->advance();
        }

        $progressBar->finish();
        $this->writeEmpty(2);
        $output->writeln('Unit ready.');

        return 0;
    }
}
