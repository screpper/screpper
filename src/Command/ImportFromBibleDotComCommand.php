<?php

declare(strict_types=1);

namespace Screpper\Command;

use Screpper\Extractor\BibleDotComExtractor;
use Screpper\Service\BibleDotComReferenceService;
use Screpper\Service\BibleInsertService;
use Screpper\Service\HttpService;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This command scrapes bible.com and saves bible verses by chapter.
 */
class ImportFromBibleDotComCommand extends AbstractCommand
{
    public const BIBLE_HERZIENE_STATENVERTALING = '1990';

    public const API_ITEMS = 'items';
    public const API_ITEM_READABLE = 'human';
    public const API_ITEM_EXTERNAL = 'usfm';

    private $bibleExternalToInternalMapping = [
        self::BIBLE_HERZIENE_STATENVERTALING => 1,
    ];

    private $booksToProcess = [
        'Genesis',
        'Exodus',
        'Leviticus',
        'Numeri',
        'Deuteronomium',
        'Jozua',
        'Richteren',
        'Ruth',
        '1 Samuel',
        '2 Samuel',
        '1 Koningen',
        '2 Koningen',
        '1 Kronieken',
        '2 Kronieken',
        'Ezra',
        'Nehemia',
        'Esther',
        'Job',
        'Psalm',
        'Spreuken',
        'Prediker',
        'Hooglied',
        'Jesaja',
        'Jeremia',
        'Klaagliederen',
        'Ezechiël',
        'Daniël',
        'Hosea',
        'Joël',
        'Amos',
        'Obadja',
        'Jona',
        'Micha',
        'Nahum',
        'Habakuk',
        'Zefanja',
        'Haggaï',
        'Zacharia',
        'Maleachi',
        'Mattheüs',
        'Markus',
        'Lukas',
        'Johannes',
        'Handelingen',
        'Romeinen',
        '1 Korinthe',
        '2 Korinthe',
        'Galaten',
        'Efeze',
        'Filippenzen',
        'Kolossenzen',
        '1 Thessalonicenzen',
        '2 Thessalonicenzen',
        '1 Timotheüs',
        '2 Timotheüs',
        'Titus',
        'Filemon',
        'Hebreeën',
        'Jakobus',
        '1 Petrus',
        '2 Petrus',
        '1 Johannes',
        '2 Johannes',
        '3 Johannes',
        'Judas',
        'Openbaring',
    ];

    /** @var \Screpper\Service\HttpService */
    private $httpService;

    /** @var \Screpper\Extractor\BibleDotComExtractor */
    private $bibleDotComExtractor;

    /** @var \Screpper\Service\BibleInsertService */
    private $bibleInsertService;

    /** @var \Screpper\Service\BibleDotComReferenceService */
    private $bibleDotComReferenceService;

    public function __construct(
        HttpService $httpService,
        BibleDotComExtractor $bibleDotComExtractor,
        BibleInsertService $bibleInsertService,
        BibleDotComReferenceService $bibleDotComReferenceService
    ) {
        parent::__construct();

        $this->bibleDotComExtractor = $bibleDotComExtractor;
        $this->httpService = $httpService;
        $this->bibleInsertService = $bibleInsertService;
        $this->bibleDotComReferenceService = $bibleDotComReferenceService;
    }

    protected function configure()
    {
        $this
            ->setName('screpper:import:bible-dot-com')
            ->setDescription('Import Bible verses from bible.com.');
    }

    /**
     * @throws \ErrorException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $output->writeln("\n<fg=black;bg=cyan>screpper bible.com importer</fg=black;bg=cyan>");
        $this->writeEmpty();
        $output->writeln('Building...');
        $this->writeEmpty();

        $books = $this->httpService->jsonGet('https://www.bible.com/json/bible/books/' . static::BIBLE_HERZIENE_STATENVERTALING, static::API_ITEMS);
        foreach ($books as $book) {

            $bookIdReadable = $book[static::API_ITEM_READABLE];
            $bookIdExternal = $book[static::API_ITEM_EXTERNAL];

            if (!in_array($bookIdReadable, $this->booksToProcess)) {
                continue;
            }

//            if ($bookIdReadable !== 'Genesis') {
//                continue;
//            }

            $output->writeln('Processing book: ' . $bookIdReadable);

            $chapters = $this->httpService->jsonGet(sprintf('https://www.bible.com/json/bible/books/%s/%s/chapters', static::BIBLE_HERZIENE_STATENVERTALING, $bookIdExternal), static::API_ITEMS);

            $progressBar = new ProgressBar($output, count($chapters));
            $progressBar->start();

            foreach ($chapters as $chapter) {

                $chapterExternalId = $chapter[static::API_ITEM_EXTERNAL];
                $chapterReadableId = (int) $chapter[static::API_ITEM_READABLE];

//                if ($chapterReadableId !== 1) {
//                    continue;
//                }

                $chapterExternalHtml = $this->httpService->htmlGet(sprintf('https://www.bible.com/nl/bible/%s/%s', static::BIBLE_HERZIENE_STATENVERTALING, $chapterExternalId));

                $bibleBook = $this->bibleInsertService->getBookByAbbreviation($bookIdReadable);

                $chapterInArray = $this->bibleDotComExtractor->extractChapterInArray($chapterExternalHtml, $chapterReadableId, $bibleBook->getId());
                $bible = $this->bibleInsertService->insertChapter(
                    $this->bibleExternalToInternalMapping[static::BIBLE_HERZIENE_STATENVERTALING],
                    $bibleBook,
                    $chapterReadableId,
                    $chapterInArray
                );

                $this->bibleDotComReferenceService->processReferencesByChapter(
                    $bible,
                    $this->bibleDotComExtractor->getVerseReferencesByChapter($chapterExternalHtml, $chapterReadableId)
                );

                usleep(1000000);
                $progressBar->advance();
            }

            $progressBar->finish();
            $this->writeEmpty();
        }

        $this->writeEmpty();
        $output->writeln('Unit ready.');

        return 0;
    }
}
