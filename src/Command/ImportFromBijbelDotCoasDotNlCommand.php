<?php

declare(strict_types=1);

namespace Screpper\Command;

use Screpper\Extractor\CoasDotNlExtractor;
use Screpper\Service\BibleInsertService;
use Screpper\Service\BibleMarginalService;
use Screpper\Service\HttpService;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * This command scrapes the Statenvertaling from bijbel.coas.nl including 'kanttekeningen'.
 */
class ImportFromBijbelDotCoasDotNlCommand extends AbstractCommand
{
    /** @var \Screpper\Service\HttpService */
    private $httpService;

    /** @var \Screpper\Service\BibleInsertService */
    private $bibleInsertService;

    /** @var \Screpper\Extractor\CoasDotNlExtractor */
    private $coasDotNlExtractor;

    /** @var \Screpper\Service\BibleMarginalService */
    private $bibleMarginalService;

    private $booksToProcess = [
        'gn' => 50,
        'ex' => 40,
        'lv' => 27,
        'nm' => 36,
        'dt' => 34,
        'jz' => 24,
        'ri' => 21,
        'ru' => 4,
        '1sm' => 31,
        '2sm' => 24,
        '1kn' => 22,
        '2kn' => 25,
        '1kr' => 29,
        '2kr' => 36,
        'ea' => 10,
        'ne' => 13,
        'jb' => 42,
        'ps' => 150,
        'sp' => 31,
        'pr' => 12,
        'hl' => 8,
        'js' => 66,
        'jr' => 52,
        'kt' => 5,
        'ez' => 48,
        'dn' => 12,
        'hs' => 14,
        'jl' => 3,
        'am' => 9,
        'ob' => 1,
        'jn' => 4,
        'mi' => 7,
        'na' => 3,
        'hk' => 3,
        'zf' => 3,
        'hg' => 2,
        'zc' => 14,
        'ml' => 4,
        'mt' => 28,
        'mk' => 16,
        'lk' => 24,
        'jh' => 21,
        'hd' => 28,
        'rm' => 16,
        '1ko' => 16,
        '2ko' => 13,
        'gl' => 6,
        'ef' => 6,
        'fl' => 4,
        'ko' => 4,
        '1th' => 5,
        '2th' => 3,
        '1tm' => 6,
        '2tm' => 4,
        'tt' => 3,
        'fm' => 1,
        'hb' => 13,
        'jk' => 5,
        '1pt' => 5,
        '2pt' => 3,
        '1jh' => 5,
        '2jh' => 1,
        '3jh' => 1,
        'jd' => 1,
        'op' => 22,
    ];

    public function __construct(
        HttpService $httpService,
        BibleInsertService $bibleInsertService,
        CoasDotNlExtractor $coasDotNlExtractor,
        BibleMarginalService $bibleMarginalService
    ) {
        parent::__construct();

        $this->httpService = $httpService;
        $this->bibleInsertService = $bibleInsertService;
        $this->coasDotNlExtractor = $coasDotNlExtractor;
        $this->bibleMarginalService = $bibleMarginalService;
    }

    protected function configure()
    {
        $this
            ->setName('screpper:import:coas-dot-nl')
            ->setDescription('Import Statenvertaling with kanttekeningen from bijbel.coas.nl.');
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;

        $output->writeln("\n<fg=black;bg=cyan>screpper bijbel.coas.nl importer</fg=black;bg=cyan>");
        $this->writeEmpty();
        $output->writeln('Building...');
        $this->writeEmpty();

        foreach ($this->booksToProcess as $bookAbbreviation => $chapters) {
            $bibleBook = $this->bibleInsertService->getBookByAbbreviation($bookAbbreviation);

            $output->writeln('Processing book: ' . $bibleBook->getName());
            $progressBar = new ProgressBar($output, $chapters);
            $progressBar->start();

            for ($i = 1; $i <= $chapters; $i++) {
                $chapterHtml = $this->httpService->htmlGet(sprintf('https://bijbel.coas.nl/BijbelMetKantTekeningen/%s_%s.html', $bookAbbreviation, $i));
                $chapterInArray = $this->coasDotNlExtractor->extractChapter($chapterHtml);

                $this->bibleInsertService->insertChapter(2, $bibleBook, $i, $chapterInArray);

                $marginaliaHtml = $this->httpService->htmlGet(sprintf('https://bijbel.coas.nl/BijbelMetKantTekeningen/kt-%s_%s.html', $bookAbbreviation, $i));
                $marginalia = $this->coasDotNlExtractor->extractMarginaliaByChapter($marginaliaHtml);

                $this->bibleMarginalService->insertMarginal(2, $bibleBook, $i, $marginalia);

                usleep(1000000);
                $progressBar->advance();
            }

            break;
            $progressBar->finish();
            $this->writeEmpty();
        }

        $this->writeEmpty();
        $output->writeln('Unit ready.');

        return 0;
    }
}
