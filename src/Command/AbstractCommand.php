<?php

declare(strict_types=1);

namespace Screpper\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

abstract class AbstractCommand extends Command
{
    /** @var \Symfony\Component\Console\Input\InputInterface */
    protected $input;

    /** @var \Symfony\Component\Console\Output\OutputInterface */
    protected $output;

    /**
     * Output empty line.
     */
    protected function writeEmpty($number = 1)
    {
        for ($i = 1; $i <= $number; $i++) {
            $this->output->writeln("");
        }
    }

    /**
     * Ask user input on the Translation (short) Indicator.
     */
    protected function defaultInputQuestion(string $question): string
    {
        $helper = $this->getHelper('question');
        $question = new Question($question);

        return $helper->ask($this->input, $this->output, $question);
    }

    /**
     * A default confirmation question.
     */
    protected function defaultConfirmationQuestion(string $question, bool $defaultChoice = false): bool
    {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion($question, $defaultChoice);

        if (false === $helper->ask($this->input, $this->output, $question)) {
            return false;
        }

        return true;
    }
}
