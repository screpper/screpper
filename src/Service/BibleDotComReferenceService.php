<?php

declare(strict_types=1);

namespace Screpper\Service;

use Screpper\Entity\Bible;
use Screpper\Entity\Bible\Reference;
use Screpper\Extractor\BibleSearchStringExtractor;
use Screpper\Model\BibleSearch;
use Screpper\Repository\Bible\ReferenceRepository;

class BibleDotComReferenceService
{
    /** @var \Screpper\Extractor\BibleSearchStringExtractor */
    private $bibleSearchStringExtractor;

    /** @var \Screpper\Repository\Bible\ReferenceRepository */
    private $referenceRepository;

    public function __construct(
        BibleSearchStringExtractor $bibleSearchStringExtractor,
        ReferenceRepository $referenceRepository
    ) {
        $this->bibleSearchStringExtractor = $bibleSearchStringExtractor;
        $this->referenceRepository = $referenceRepository;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function processReferencesByChapter(Bible $bibleChapter, array $referencesForChapter)
    {
        $bibleSearchesByVerse = [];

        $this->referenceRepository->removeByChapter($bibleChapter);

        foreach ($referencesForChapter as $verse => $referencesForVerse) {
            $bibleSearchesByVerse[$verse] = $this->extractReferencesForVerse($referencesForVerse);
        }

        foreach ($bibleSearchesByVerse as $verse => $bibleSearchByVerse) {
            /** @var \Screpper\Model\BibleSearch $bibleSearch */
            foreach ($bibleSearchByVerse as $bibleSearch) {
                $bibleReference = (new Reference())
                    ->setTranslation($bibleChapter->getTranslation())
                    ->setBook($bibleChapter->getBook())
                    ->setChapter($bibleChapter->getChapter())
                    ->setVerse($verse)
                    ->setReference($bibleSearch->formatSearchString());

                $this->referenceRepository->persist($bibleReference);
            }
        }

        $this->referenceRepository->flush();
    }

    /**
     * @return \Screpper\Model\BibleSearch[]
     */
    private function extractReferencesForVerse(array $referencesForVerse): array
    {
        $bibleSearches = [];

        foreach ($referencesForVerse as $referenceForVerse) {
            $explodedReferences = explode(' ', trim($referenceForVerse));
            $book = array_shift($explodedReferences);
            if (is_numeric($book)) {
                $book = sprintf('%s %s', $book, array_shift($explodedReferences));
            }

            foreach ($explodedReferences as $explodedReference) {
                // Check if comma separated verses present.
                if (strpos($explodedReference, ',') > 0) {
                    if (!strpos($explodedReference, ':')) {
                        $this->extractMultipleChapters($explodedReference, $bibleSearches, $book);
                    } else {
                        $this->extractMultipleVerses($explodedReference, $bibleSearches, $book);
                    }
                } else {
                    $bibleSearches[] = $this->bibleSearchStringExtractor->extractSearchString(
                        (new BibleSearch())->setSearchString(sprintf('%s %s', $book, $explodedReference))
                    );
                }
            }
        }

        return $bibleSearches;
    }

    private function extractMultipleVerses(string $explodedReference, array &$bibleSearches, string $book)
    {
        $explodedVersesByComma = explode(',', $explodedReference);
        // first part is chapter + first verse
        $explodedChapterAndFirstVerse = explode(':', array_shift($explodedVersesByComma));
        $explodedChapter = $explodedChapterAndFirstVerse[0];
        $explodedVerse = $explodedChapterAndFirstVerse[1];

        $explodedVersesByComma = array_merge([$explodedVerse], $explodedVersesByComma);

        // Check if there are sequential verses as reference.
        $verseSets = $this->extractSequential($explodedVersesByComma, $explodedVerse);

        foreach ($verseSets as $verseSet) {
            $bibleSearches[] = $this->bibleSearchStringExtractor->extractSearchString(
                (new BibleSearch())->setSearchString(sprintf('%s %s:%s', $book, $explodedChapter, $verseSet))
            );
        }
    }

    private function extractMultipleChapters(string $explodedReference, array &$bibleSearches, string $book)
    {
        $explodedChaptersByComma = explode(',', $explodedReference);

        // Check if there are sequential verses as reference.
        $firstChapter = $explodedChaptersByComma[0];
        $chapterSets = $this->extractSequential($explodedChaptersByComma, $firstChapter);

        foreach ($chapterSets as $chapterSet) {
            $bibleSearches[] = $this->bibleSearchStringExtractor->extractSearchString(
                (new BibleSearch())->setSearchString(sprintf('%s %s', $book, $chapterSet))
            );
        }
    }

    private function extractSequential(array $numerals, string $startingNumeral): array
    {
        $highestInSequence = 0;
        $numeralSets = [];

        foreach ($numerals as $key => $verse) {
            $prevKey = $key - 1;
            if (!array_key_exists($prevKey, $numerals)) {
                continue;
            }

            $prevValue = (int) $numerals[$prevKey];
            $currentValue = (int) $verse;

            if ($currentValue !== $prevValue + 1) {
                if ($highestInSequence > 0) {
                    $numeralSets[] = sprintf('%s-%s', $startingNumeral, $prevValue);
                }
                $startingNumeral = $currentValue;

                continue;
            }

            $highestInSequence = $currentValue;
        }

        if ($highestInSequence > 0) {
            if ($highestInSequence < $startingNumeral) {
                $numeralSets[] = $startingNumeral;
            } else {
                $numeralSets[] = sprintf('%s-%s', $startingNumeral, $highestInSequence);
            }
        }

        return $numeralSets;
    }
}
