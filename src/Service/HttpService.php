<?php

declare(strict_types=1);

namespace Screpper\Service;

use Symfony\Component\HttpClient\HttpClient;

class HttpService
{
    /** @var \Symfony\Contracts\HttpClient\HttpClientInterface */
    private $httpClient;

    public function __construct()
    {
        $this->httpClient = HttpClient::create();
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \RuntimeException
     */
    public function jsonGet(string $url, $responseDataKey = null): array
    {
        $response = $this->httpClient->request('GET', $url);
        $content = $response->toArray();
        if (empty($content)) {
            throw new \RuntimeException('Empty response for GET ' . $url);
        }

        if (null !== $responseDataKey && isset($content[$responseDataKey]) && !empty($content[$responseDataKey])) {
            return $content[$responseDataKey];
        }

        throw new \RuntimeException('Empty or non-existing response data key for GET ' . $url);
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \RuntimeException
     */
    public function htmlGet(string $url): string
    {
        $response = $this->httpClient->request('GET', $url);
        $content = $response->getContent();
        if (empty($content)) {
            throw new \RuntimeException('Empty response for GET ' . $url);
        }

        return $content;
    }
}
