<?php

declare(strict_types=1);

namespace Screpper\Service;

use Screpper\Entity\Bible;
use Screpper\Repository\Bible\KeyWordRepository;

class BibleKeyWordService
{
    /** @var \Screpper\Repository\Bible\KeyWordRepository */
    private $keyWordRepository;

    public function __construct(
        KeyWordRepository $keyWordRepository
    ) {
        $this->keyWordRepository = $keyWordRepository;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function processKeyWordsByChapter(Bible $bible, array $keyWordsByChapter)
    {
        foreach ($keyWordsByChapter as $verse => $keyWordsByVerse) {
            foreach ($keyWordsByVerse as $keyWordByVerse) {
                if (strpos($keyWordByVerse, '-') === 0 || in_array($keyWordByVerse, ['\'n', '\'s', '-'])) {
                    continue;
                }
                preg_match_all('/^[0-9]*$/m', $keyWordByVerse, $matches, PREG_SET_ORDER, 0);
                if (count($matches) > 0) {
                    continue;
                }

                $searchString = sprintf('%s %s:%s', $bible->getBook()->getName(), $bible->getChapter(), $verse);

                $keyWord = $this->keyWordRepository->findRaw($bible->getTranslation(), $keyWordByVerse);
                if (null === $keyWord) {
                    $this->keyWordRepository->insert(
                        $bible->getTranslation(),
                        $keyWordByVerse,
                        [$searchString]
                    );

                    continue;
                }

                if (null !== $keyWord['search_strings']) {
                    $searchStrings = json_decode($keyWord['search_strings']);
                } else {
                    var_dump($keyWord);
                    $searchStrings = [];
                }
                if (!in_array($searchString, $searchStrings)) {
                    $this->keyWordRepository->update($keyWord['id'], array_merge($searchStrings, [$searchString]));
                }
            }
        }
    }
}
