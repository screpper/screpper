<?php

declare(strict_types=1);

namespace Screpper\Service;

use Screpper\Entity\Bible\Book;
use Screpper\Entity\Bible\Marginal;
use Screpper\Entity\Bible\Translation;
use Screpper\Repository\Bible\MarginalRepository;
use Screpper\Repository\Bible\TranslationRepository;

class BibleMarginalService
{
    /** @var \Screpper\Repository\Bible\MarginalRepository */
    private $marginalRepository;

    /** @var \Screpper\Repository\Bible\TranslationRepository */
    private $translationRepository;

    public function __construct(
        MarginalRepository $marginalRepository,
        TranslationRepository $translationRepository
    ) {
        $this->marginalRepository = $marginalRepository;
        $this->translationRepository = $translationRepository;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function insertMarginal(
        int $translationId,
        Book $book,
        int $chapter,
        array $marginals
    ): Marginal {
        $translation = $this->getTranslationById($translationId);
        $this->marginalRepository->removeByTranslationBookAndChapter($translation, $book, $chapter);

        $marginal = (new Marginal())
            ->setTranslation($translation)
            ->setBook($book)
            ->setChapter($chapter)
            ->setMargins($marginals);

        $this->marginalRepository->persist($marginal);
        $this->marginalRepository->flush();

        return $marginal;
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function getTranslationById(int $id): Translation
    {
        $translation = $this->translationRepository->find($id);
        if (null === $translation) {
            throw new \InvalidArgumentException(sprintf('Translation %d not found.', $id));
        }

        return $translation;
    }
}
