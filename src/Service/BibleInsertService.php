<?php

declare(strict_types=1);

namespace Screpper\Service;

use Screpper\Entity\Bible;
use Screpper\Entity\Bible\Book;
use Screpper\Entity\Bible\Translation;
use Screpper\Repository\Bible\AbbreviationRepository;
use Screpper\Repository\Bible\TranslationRepository;
use Screpper\Repository\BibleRepository;

class BibleInsertService
{
    /** @var \Screpper\Repository\BibleRepository */
    private $bibleRepository;

    /** @var \Screpper\Repository\Bible\AbbreviationRepository */
    private $abbreviationRepository;

    /** @var \Screpper\Repository\Bible\TranslationRepository */
    private $translationRepository;

    public function __construct(
        BibleRepository $bibleRepository,
        AbbreviationRepository $abbreviationRepository,
        TranslationRepository $translationRepository
    ) {
        $this->bibleRepository = $bibleRepository;
        $this->abbreviationRepository = $abbreviationRepository;
        $this->translationRepository = $translationRepository;
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function insertChapter(
        int $translationId,
        Book $book,
        int $chapter,
        array $text
    ): Bible {
        $translation = $this->getTranslationById($translationId);
        $this->bibleRepository->removeByTranslationBookAndChapter($translation, $book, $chapter);

        $bible = (new Bible())
            ->setTranslation($translation)
            ->setBook($book)
            ->setChapter($chapter)
            ->setText($text);

        $this->bibleRepository->persist($bible);
        $this->bibleRepository->flush();

        return $bible;
    }

    /**
     * @throws \InvalidArgumentException
     */
    public function getBookByAbbreviation(string $abbreviationToFind): Book
    {
        $abbreviation = $this->abbreviationRepository->find($abbreviationToFind);
        if (null === $abbreviation) {
            throw new \InvalidArgumentException(sprintf('Abbreviation %s not found.', $abbreviationToFind));
        }

        return $abbreviation->getBook();
    }

    /**
     * @throws \InvalidArgumentException
     */
    private function getTranslationById(int $id): Translation
    {
        $translation = $this->translationRepository->find($id);
        if (null === $translation) {
            throw new \InvalidArgumentException(sprintf('Translation %d not found.', $id));
        }

        return $translation;
    }
}
