<?php

declare(strict_types=1);

namespace Screpper\Service;

use Screpper\Extractor\BibleSearchStringExtractor;
use Screpper\Model\BibleSearch;
use Screpper\Repository\Bible\AbbreviationRepository;
use Screpper\Repository\BibleRepository;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class BibleSearchService
{
    /** @var \Screpper\Extractor\BibleSearchStringExtractor */
    private $bibleSearchStringExtractor;

    /** @var \Screpper\Repository\BibleRepository */
    private $bibleRepository;

    /** @var \Screpper\Repository\Bible\AbbreviationRepository */
    private $abbreviationRepository;

    public function __construct(
        BibleSearchStringExtractor $bibleSearchStringExtractor,
        BibleRepository $bibleRepository,
        AbbreviationRepository $abbreviationRepository
    ) {
        $this->bibleSearchStringExtractor = $bibleSearchStringExtractor;
        $this->bibleRepository = $bibleRepository;
        $this->abbreviationRepository = $abbreviationRepository;
    }

    /**
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function searchBySearchForm(BibleSearch $bibleSearch): array
    {
        $bibleSearch = $this->bibleSearchStringExtractor->extractSearchString($bibleSearch);
        $objectNormalizer = new ObjectNormalizer();

        $result = [];
        foreach ($bibleSearch->getTranslations() as $translation) {
            $result[$translation->getId()] = ['translation' => $objectNormalizer->normalize($translation)];

            $book = $this->abbreviationRepository->find($bibleSearch->getBook());
            if (null === $book) {
                continue;
            }

            $bibleChapter = $this->bibleRepository->findBy([
                'translation' => $translation,
                'book' => $book->getBook(),
                'chapter' => $bibleSearch->getChapter(),
            ]);

            dump($bibleChapter);
        }

        return array_values($result);
    }
}
