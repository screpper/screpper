<?php

namespace Screpper\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

class BibleSearch
{
    /**
     * @var string
     *
     *  @Assert\NotNull()
     */
    private $searchString;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection|null
     *
     * @Assert\NotNull()
     */
    private $translations;

    /** @var string */
    private $book;

    /** @var int */
    private $chapter;

    /** @var int */
    private $verseFrom;

    /** @var int */
    private $verseToOrChapterTo;

    /** @var int */
    private $verseToForChapterTo;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getSearchString(): ?string
    {
        return $this->searchString;
    }

    public function setSearchString(string $searchString): BibleSearch
    {
        $this->searchString = $searchString;

        return $this;
    }

    public function hasSearchString(): bool
    {
        return !empty($this->searchString);
    }

    /**
     * @return \Screpper\Entity\Bible\Translation[]
     */
    public function getTranslations(): ?ArrayCollection
    {
        return $this->translations;
    }

    public function setTranslations(?ArrayCollection $translations): BibleSearch
    {
        $this->translations = $translations;

        return $this;
    }

    public function getBook(): string
    {
        return $this->book;
    }

    public function setBook(string $book): BibleSearch
    {
        $this->book = $book;

        return $this;
    }

    public function getChapter(): int
    {
        return $this->chapter;
    }

    public function setChapter(int $chapter): BibleSearch
    {
        $this->chapter = $chapter;

        return $this;
    }

    public function getVerseFrom(): int
    {
        return $this->verseFrom;
    }

    public function setVerseFrom(int $verseFrom): BibleSearch
    {
        $this->verseFrom = $verseFrom;

        return $this;
    }

    public function getVerseToOrChapterTo(): int
    {
        return $this->verseToOrChapterTo;
    }

    public function setVerseToOrChapterTo(int $verseToOrChapterTo): BibleSearch
    {
        $this->verseToOrChapterTo = $verseToOrChapterTo;

        return $this;
    }

    public function getVerseToForChapterTo(): int
    {
        return $this->verseToForChapterTo;
    }

    public function setVerseToForChapterTo(int $verseToForChapterTo): BibleSearch
    {
        $this->verseToForChapterTo = $verseToForChapterTo;

        return $this;
    }

    public function formatSearchString(): string
    {
        $formatted = sprintf('%s %s:%s', $this->getBook(), $this->getChapter(), $this->getVerseFrom());

        if ($this->getVerseFrom() === $this->getVerseToOrChapterTo()) {
            return $formatted;
        }

        if (0 === $this->getVerseToOrChapterTo()) {
            // No ending verse has been found, return nothing
            return $formatted;
        }

        $formatted .= '-' . $this->getVerseToOrChapterTo();

        if (!empty($this->getVerseToForChapterTo())) {
            $formatted .= ':' . $this->getVerseToForChapterTo();
        }

        return $formatted;
    }
}
