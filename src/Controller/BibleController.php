<?php

declare(strict_types=1);

namespace Screpper\Controller;

use Screpper\Form\BibleSearchFormType;
use Screpper\Model\BibleSearch;
use Screpper\Response\ResponseBag;
use Screpper\Service\BibleSearchService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BibleController extends AbstractController
{
    /** @var \Screpper\Service\BibleSearchService */
    private $bibleSearchService;

    public function __construct(
        BibleSearchService $bibleSearchService
    ) {
        $this->bibleSearchService = $bibleSearchService;
    }

    /**
     * @Route("/api/bible", methods={"GET"})
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getListAction(Request $request): JsonResponse
    {
        $form = $this->createForm(BibleSearchFormType::class, new BibleSearch());
        $form->submit($request->query->all());

        if (!$form->isValid()) {
            return new JsonResponse((new ResponseBag())->addFormErrors($form), JsonResponse::HTTP_BAD_REQUEST);
        }

        /** @var \Screpper\Model\BibleSearch $bibleSearch */
        $bibleSearch = $form->getData();

        $data = $this->bibleSearchService->searchBySearchForm($bibleSearch);

        return new JsonResponse((new ResponseBag())->setData($data));
    }
}
