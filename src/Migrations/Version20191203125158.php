<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191203125158 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bible_key_words (id INT AUTO_INCREMENT NOT NULL, bible_translation_id INT DEFAULT NULL, key_word VARCHAR(50) NOT NULL, search_strings LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', INDEX IDX_40C159328F2AA3AD (bible_translation_id), INDEX i_keyword (bible_translation_id, key_word), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bible_key_words ADD CONSTRAINT FK_40C159328F2AA3AD FOREIGN KEY (bible_translation_id) REFERENCES bible_translation (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE bible_key_words');
    }
}
