<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191202194356 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bible_references (id INT AUTO_INCREMENT NOT NULL, bible_translation_id INT DEFAULT NULL, bible_book_id INT DEFAULT NULL, chapter SMALLINT NOT NULL, verse SMALLINT NOT NULL, reference VARCHAR(50) NOT NULL, type VARCHAR(10) NOT NULL, votes SMALLINT NOT NULL, INDEX IDX_B8CFEE0C8F2AA3AD (bible_translation_id), INDEX IDX_B8CFEE0C67FFD77 (bible_book_id), INDEX i_verse (bible_book_id, chapter, verse, bible_translation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bible_references ADD CONSTRAINT FK_B8CFEE0C8F2AA3AD FOREIGN KEY (bible_translation_id) REFERENCES bible_translation (id)');
        $this->addSql('ALTER TABLE bible_references ADD CONSTRAINT FK_B8CFEE0C67FFD77 FOREIGN KEY (bible_book_id) REFERENCES bible_book (id)');
        $this->addSql('DROP TABLE document');
        $this->addSql('CREATE INDEX i_chapter ON bible (bible_translation_id, bible_book_id, chapter)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, data LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE bible_references');
        $this->addSql('DROP INDEX i_chapter ON bible');
    }
}
