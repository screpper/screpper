<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191127211658 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bible_translation (id INT NOT NULL, name_short VARCHAR(30) NOT NULL, name VARCHAR(50) NOT NULL, copyright_nl LONGTEXT DEFAULT NULL, copyright_en LONGTEXT DEFAULT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bible_abbreviation (abbreviation VARCHAR(30) NOT NULL, bible_book_id INT DEFAULT NULL, INDEX IDX_CFDF7FDC67FFD77 (bible_book_id), PRIMARY KEY(abbreviation)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bible_book (id INT NOT NULL, testament VARCHAR(5) NOT NULL, name VARCHAR(50) NOT NULL, chapters SMALLINT NOT NULL, UNIQUE INDEX bible_book_name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bible (id INT AUTO_INCREMENT NOT NULL, bible_translation_id INT DEFAULT NULL, bible_book_id INT DEFAULT NULL, chapter SMALLINT NOT NULL, verse SMALLINT NOT NULL, text LONGTEXT NOT NULL, INDEX IDX_DC3DC58B8F2AA3AD (bible_translation_id), INDEX IDX_DC3DC58B67FFD77 (bible_book_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bible_abbreviation ADD CONSTRAINT FK_CFDF7FDC67FFD77 FOREIGN KEY (bible_book_id) REFERENCES bible_book (id)');
        $this->addSql('ALTER TABLE bible ADD CONSTRAINT FK_DC3DC58B8F2AA3AD FOREIGN KEY (bible_translation_id) REFERENCES bible_translation (id)');
        $this->addSql('ALTER TABLE bible ADD CONSTRAINT FK_DC3DC58B67FFD77 FOREIGN KEY (bible_book_id) REFERENCES bible_book (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bible DROP FOREIGN KEY FK_DC3DC58B8F2AA3AD');
        $this->addSql('ALTER TABLE bible_abbreviation DROP FOREIGN KEY FK_CFDF7FDC67FFD77');
        $this->addSql('ALTER TABLE bible DROP FOREIGN KEY FK_DC3DC58B67FFD77');
        $this->addSql('DROP TABLE bible_translation');
        $this->addSql('DROP TABLE bible_abbreviation');
        $this->addSql('DROP TABLE bible_book');
        $this->addSql('DROP TABLE bible');
    }
}
