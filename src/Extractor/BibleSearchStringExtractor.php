<?php

declare(strict_types=1);

namespace Screpper\Extractor;

use Screpper\Model\BibleSearch;

class BibleSearchStringExtractor
{
    /**
     * Add and return query parameters to a model.
     */
    public function extractSearchString(BibleSearch $bibleSearch): ?BibleSearch
    {
        if (!$bibleSearch->hasSearchString()) {
            return null;
        }

        /*
         * Regular expression to match bible verse searches.
         * Examples:
         *
         * Joh. 1:1,2       [1]   [2] Joh  [3] 1  [4] 1  [5] 2
         * John 14:1-15:2   [1]   [2] John [3] 14 [4] 1  [5] 15 [6] 2
         * Mat. 5:1-12      [1]   [2] Mat  [3] 5  [4] 1  [5] 12
         * John14:　　　16   [1]   [2] John [3] 14 [4] 16
         * 1 Tess 1:2,3     [1] 1 [2] Tess [3] 1  [4]    [5] 3
         * 2joh 1:2         [1] 2 [2] joh  [3] 1  [4] 2
         * Is 4:15-17       [1]   [2] Is   [3] 4  [4] 15 [5] 17
         * monk honk        [1]   [2] monk
         *
         * [1] Bible book number, numeric match, e.g. the "1" of "1 John"
         * [2] Bible book match, numeric and All chars match, e.g. "Genesis" or "Pet"
         * [3] Numeric match for bible book chapter
         * [4] Numeric match for bible book chapter verse 'from' or single verse
         * [5] Numeric match for bible verse 'to' or when selecting two (Joh. 2:2,3) or (Joh. 2:2-15)
         * [6] Numeric match for bible verse, e.g. (John 14:1-15:2 (the 2))
         *
         *          [1]         [2]                      [3]                     [4]                       [5]                     [6]
         */
//        $regEx = "/^([\\d]\\s*)?(\\p{N}?\\p{L}+)\\.?\\s*(\\d+)?[\\p{Pd}\\p{Zs}:]*(\\d+)?[\\p{Pd}\\p{Zs},]*(\\d+)?[\\p{Pd}\\p{Zs}:]*(\\d+)?/m";
        $regEx = "/^([\\d]\\s*)?(\\p{N}?\\p{L}+)\\.?\s*(\\d+)?[\\p{Pd}\\p{Zs}:]*(\\d+)?[\\p{Pd}\\p{Zs},]*(\\d+)?[\\p{Pd}\\p{Zs},]*(\\d+)?[\\p{Pd}\\p{Zs},]*(\\d+)?[\\p{Pd}\\p{Zs},]*(\\d+)?[\\p{Pd}\\p{Zs}:]*(\\d+)?/m";
        preg_match_all($regEx, $this->unAccent($bibleSearch->getSearchString()), $parts);
        if (empty($parts[0])) {
            return null;
        }

        $bibleSearch
            ->setBook($parts[1][0] . $parts[2][0])
            ->setChapter((int) $parts[3][0])
            ->setVerseFrom((int) $parts[4][0])
            ->setVerseToOrChapterTo((int) $parts[5][0])
            ->setVerseToForChapterTo((int) $parts[6][0]);

        if (empty($bibleSearch->getChapter())) {
            $bibleSearch
                ->setChapter(1)
                ->setVerseFrom(1)
                ->setVerseToOrChapterTo(999);
        }

        if (!empty($bibleSearch->getChapter()) && empty($bibleSearch->getVerseFrom())) {
            $bibleSearch
                ->setVerseFrom(1)
                ->setVerseToOrChapterTo(999);
        }

        if (false === strpos($bibleSearch->getSearchString(), ':') &&
            strpos($bibleSearch->getSearchString(), '-') > 0
        ) {
            $bibleSearch
                ->setVerseToOrChapterTo((int) $bibleSearch->getVerseFrom())
                ->setVerseFrom(1)
                ->setVerseToForChapterTo(999);
        }

        return $bibleSearch;
    }

    private function unAccent($string): string
    {
        if (strpos($string = htmlentities($string, ENT_QUOTES, 'UTF-8'), '&') !== false) {
            return html_entity_decode(
                preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|tilde|uml);~i', '$1', $string),
                ENT_QUOTES,
                'UTF-8'
            );
        }

        return $string;
    }
}
