<?php

declare(strict_types=1);

namespace Screpper\Extractor;

use DonatelloZa\RakePlus\RakePlus;
use DonatelloZa\RakePlus\StopwordsPatternFile;
use Symfony\Component\DomCrawler\Crawler;

class BibleDotComExtractor
{
    /** @var \DonatelloZa\RakePlus\StopwordsPatternFile */
    private $stopWordsPattern;

    public function __construct()
    {
        $this->stopWordsPattern = StopwordsPatternFile::create(__DIR__ . '/../../resources/nl_NL.pattern');
    }

    /**
     * @throws \ErrorException
     */
    public function extractChapterInArray(string $htmlString, int $chapterNumber, int $bookId): array
    {
        $verses = [];
        $versesFlat = [];
        $keyWords = [];

        $crawler = new Crawler($htmlString);
        $chapterCrawler = new Crawler($crawler->filterXPath(sprintf('//*[@class="chapter ch%s"]', $chapterNumber))->first()->html());

        $versesInChapter = $this->getVersesInChapterFromCrawler($chapterCrawler);
        foreach ($versesInChapter as $verseInChapter) {
            $verse = $this->extractVerse($chapterCrawler, $verseInChapter, $chapterNumber, $bookId);
            $verses[$verseInChapter] = $verse;
            $versesFlat[$verseInChapter] = $this->getFlatVerse($verse);
            $keyWords[$verseInChapter] = RakePlus::create($versesFlat[$verseInChapter], $this->stopWordsPattern)->keywords();
        }

        $versesHtml = $verses;
        $this->addBreaks($versesHtml, $this->getBreaks($chapterCrawler));
        $this->addHeaders($versesHtml, $this->getHeaders($chapterCrawler));

        return [
            'headers' => $this->getHeaders($chapterCrawler),
            'breaks' => $this->getBreaks($chapterCrawler),
            'verses' => $verses,
            'versesFlat' => $versesFlat,
            'chapterHtml' => implode('', $versesHtml),
            'keyWords' => $keyWords,
        ];
    }

    public function getHeaders(Crawler $crawler): array
    {
        $headers = [];
        $crawler->filterXPath('//*[@class="s"]')->each(function (Crawler $headerCrawler) use (&$headers) {
            $label = $headerCrawler->nextAll()->filterXPath('//*[@class="label"]')->text();
            if ($label === '#') {
                $label = $headerCrawler->previousAll()->filterXPath('//*[@class="label"]')->text();
            }
            $headers[$label] = $headerCrawler->text();
        });

        return $headers;
    }

    public function getBreaks(Crawler $crawler): array
    {
        $breaks = [];
        $crawler->filterXPath('//*[@class="b"]')->each(function (Crawler $breakCrawler) use (&$breaks) {
            try {
                $breaks[] = (int) $breakCrawler->nextAll()->filterXPath('//*[@class="label"]')->text();
            } catch (\InvalidArgumentException $e) {
                return;
            }
        });

        return $breaks;
    }

    public function extractVerse(Crawler $chapterCrawler, int $verseNumber, int $chapterNumber, int $bookId)
    {
        $verse = '';
        $chapterCrawler->filterXPath(sprintf('//*[@class="verse v%s"]', $verseNumber))->each(function (Crawler $verseCrawler) use (&$verse) {

            $verseBlock = '';
            $verseCrawler->filterXPath('//span')->each(function (Crawler $verseSpanCrawler) use (&$verseBlock) {
                switch ($verseSpanCrawler->attr('class')) {
                    case 'content' :
                        $this->getVerseContent($verseSpanCrawler, $verseBlock);
                        break;
                    case 'note f':
                    case 'note x':
                        // Verse references
                        $this->getVerseReferencesByVerse($verseSpanCrawler, $verseBlock);

                        break;
                }
            });

            $verse .= $this->buildVerseBlock($verseCrawler, $verseBlock);
        });

        return sprintf('<div class="p" rel="%d_%d_%d"><span class="n">%d</span>%s</div>', $bookId, $chapterNumber, $verseNumber, $verseNumber, $verse);
    }

    public function getVersesInChapterFromHtml(string $htmlString): array
    {
        return $this->getVersesInChapterFromCrawler(new Crawler($htmlString));
    }

    public function getVerseReferencesByChapter(string $htmlString, int $chapterNumber): array
    {
        $referencesByVerse = [];

        $crawler = new Crawler($htmlString);
        $chapterCrawler = new Crawler($crawler->filterXPath(sprintf('//*[@class="chapter ch%s"]', $chapterNumber))->first()->html());

        $versesInChapter = $this->getVersesInChapterFromCrawler($chapterCrawler);
        foreach ($versesInChapter as $verseInChapter) {

            $chapterCrawler->filterXPath(sprintf('//*[@class="verse v%s"]', $verseInChapter))->each(function (Crawler $verseCrawler) use (&$referencesByVerse, $verseInChapter, $chapterNumber) {
                $verseCrawler->filterXPath('//span')->each(function (Crawler $verseSpanCrawler) use (&$referencesByVerse, $verseInChapter, $chapterNumber) {

                    if ('note x' === $verseSpanCrawler->attr('class')) {
                        $references = trim($verseSpanCrawler->text(), '#');
                        if (empty($references)) {
                            return;
                        }

                        $references = str_replace(
                            ['enz.', 'vs. ', 'Vers '],
                            ['', '1:', $chapterNumber . ':'],
                            $references
                        );
                        $splitReferences = explode('; ', $references);
                        $extractedReferences = [];
                        foreach ($splitReferences as $splitReference) {
                            $splitReference = trim($splitReference);
                            $colonPos = strpos($splitReference, ':');
                            if ($colonPos > 0 && is_numeric(substr($splitReference, 0, $colonPos))) {
                                array_push($extractedReferences, array_pop($extractedReferences) . ' ' . $splitReference);

                                continue;
                            }
                            array_push($extractedReferences, $splitReference);
                        }

                        $referencesByVerse[$verseInChapter] = $extractedReferences;
                    }
                });
            });
        }

        return $referencesByVerse;
    }

    private function getVersesInChapterFromCrawler(Crawler $crawler): array
    {
        $versesInChapter = [];
        $crawler->filterXPath('//*[contains(@class, \'verse v\')]')->each(function (Crawler $verseCrawler) use (&$versesInChapter) {
            preg_match('/\d+/', $verseCrawler->attr('class'), $matches);
            $versesInChapter[] = (int) end($matches);
        });

        return array_values(array_unique($versesInChapter));
    }

    private function getVerseContent(Crawler $crawler, string &$verseBlock)
    {
        $content = trim($crawler->text());
        if (empty($content)) {
            return;
        }

        $classes = ['v'];

        if ('Sela' === $content) {
            $classes[] = 'v-marker';
        }

        $parentCrawler = $crawler->filterXPath('//span/..');
        if ('add' === $parentCrawler->attr('class')) {
            $classes[] = 'v-add';
        }

        $verseBlock .= sprintf('<span class="%s">%s</span>', implode(' ', $classes), $content);
    }

    private function getVerseReferencesByVerse(Crawler $crawler, string &$verseBlock)
    {
        $references = trim($crawler->text());
        if (empty($references)) {
            return;
        }

        $verseBlock .= '<span class="ref-h">#</span>';
        $referenceChildren = $crawler->filterXPath('//*[@class=" body"]')->children();

        if (0 === $referenceChildren->count()) {
            $verseBlock .= '<span class="ref-c">' . trim($crawler->text(), '# ') . '</span>';
        }

        $crawler->filterXPath('//*[@class=" body"]')->children()->each(function (Crawler $verseReferenceCrawler) use (&$verseBlock) {
            $classRemap = [
                'fr' => 'ref-v',
                'fq' => 'ref-w',
                'ft' => 'ref-c',
            ];
            $verseBlock .= sprintf('<span class="%s">%s</span>', $classRemap[$verseReferenceCrawler->attr('class')], $verseReferenceCrawler->text());
        });
    }

    private function buildVerseBlock(Crawler $verseCrawler, string $verseBlock): string
    {
        $verseBlock = trim($verseBlock);
        if (empty($verseBlock)) {
            return $verseBlock;
        }

        $classes = ['l'];

        $parentClass = $verseCrawler->parents()->filterXPath('//div')->attr('class');
        if ('d' === $parentClass) {
            $classes[] = 'l-intro';
        }

        return sprintf('<div class="%s">%s</div>', implode(' ', $classes), $verseBlock);
    }

    private function getFlatVerse(string $verse): string
    {
        $verseCrawler = new Crawler($verse);
        $flatVerse = '';
        $verseCrawler->filterXPath('//*[contains(@class, "v")]')->each(function (Crawler $verseSpanCrawler) use (&$flatVerse) {
            $flatVerse .= ' ' . $verseSpanCrawler->text();
        });

        return trim(str_replace(' .', '.', $flatVerse));
    }

    /**
     * @throws \ErrorException
     */
    private function addBreaks(array &$extracted, array $breaks)
    {
        foreach ($breaks as $break) {
            if (0 === $break && strpos($extracted[1], ':') > 0) {
                str_replace(':', ':<span class="b-mof"></span>' . $extracted[1], $extracted[1]);

                continue;
            }
            if (0 === $break || 0 === $break - 1) {
                continue;
            }
            try {
                $extracted[$break - 1] = '<div class="b"></div>' . $extracted[$break - 1];
            } catch (\Throwable $t) {
                var_dump($extracted);
                throw new \ErrorException(sprintf(
                    'Message: %s; Imploded breaks: %s; Current break: %s; Extracted: %s',
                    $t->getMessage(), implode('/', $breaks), $break, implode('/', $extracted)
                ));
            }
        }
    }

    private function addHeaders(array &$extracted, array $headers)
    {
        foreach ($headers as $position => $header) {
            $header = sprintf('<div class="p p-hdr">%s</div>', $header);
            array_splice($extracted, $position - 1, 0, $header);
        }
    }
}
