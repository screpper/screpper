<?php

declare(strict_types=1);

namespace Screpper\Extractor;

use DonatelloZa\RakePlus\RakePlus;
use DonatelloZa\RakePlus\StopwordsPatternFile;
use Symfony\Component\DomCrawler\Crawler;

class CoasDotNlExtractor
{
    /** @var \DonatelloZa\RakePlus\StopwordsPatternFile */
    private $stopWordsPattern;

    public function __construct()
    {
        $this->stopWordsPattern = StopwordsPatternFile::create(__DIR__ . '/../../resources/nl_NL.pattern');
    }

    public function extractChapter(string $htmlString)
    {
        $crawler = new Crawler($htmlString);
        $chapterCrawler = new Crawler($crawler->filterXPath('//table')->first()->html());

        $chapterContent = [];

        $verse = 1;
        $chapterCrawler->filterXPath('//tr')->each(function (Crawler $verseCrawler) use (&$chapterContent, &$verse) {
            if (!strpos($verseCrawler->html(), sprintf('<a name="%s">', $verse))) {
                return;
            }

            $verseCrawler->filterXPath('//td')->last()->each(function (Crawler $verseContentCrawler) use (&$chapterContent, $verse) {

                $margins = [];
                $verseContentCrawler->filterXPath('//a')->each(function (Crawler $hrefCrawler) use (&$margins) {
                    $hrefExplode = explode('#', $hrefCrawler->attr('href'));

                    $margins[end($hrefExplode)] = $hrefCrawler->text();
                });

                $verseText = preg_replace(
                    '/\s\s+/',
                    ' ',
                    strip_tags(
                        preg_replace(
                            '#<sup>(.*?)</sup>#',
                            ' ',
                            $verseContentCrawler->html()
                        )
                    )
                );

                foreach ($margins as $marginNumber => $marginWord) {
                    $verseText = str_replace($marginWord, sprintf('<span class="m-word">%s</span><span class="m-number">%s)</span>', $marginWord, $marginNumber), $verseText);
                }

                $chapterContent[$verse] = sprintf('<span class="v">%s</span>', str_replace(array("\n", "\r"), '', trim($verseText)));
            });

            ++$verse;
        });

        $flatVerses = $this->getFlatVerses($chapterContent);

        return [
            'headers' => null,
            'breaks' => null,
            'verses' => $chapterContent,
            'versesFlat' => $flatVerses,
            'chapterHtml' => $this->getFullChapter($chapterContent),
            'keyWords' => $this->getKeyWords($flatVerses),
        ];
    }

    public function extractMarginaliaByChapter(string $htmlString): array
    {
        $crawler = new Crawler($htmlString);
        $chapterCrawler = new Crawler($crawler->filterXPath('//table')->first()->html());

        $margin = [];
        $chapterCrawler->filterXPath('//tr')->each(function (Crawler $rowCrawler) use (&$margin) {
            $rowCrawler->filterXPath('//td')->each(function (Crawler $rowItemCrawler) use (&$margin, $rowCrawler) {
                if (strpos($rowItemCrawler->html(), ')</b>') > 0) {
                    $marginNumber = explode(')', $rowItemCrawler->text());
                    $currentMarginNumber = array_shift($marginNumber);

                    $currentMarginWord = $rowItemCrawler->nextAll()->filterXPath('//td')->first()->text();
                    $marginElement = $rowCrawler->nextAll()->filterXPath('//tr')->first()->filterXPath('//td')->last();
                    $marginHtml = trim($marginElement->html());
                    $marginText = trim($marginElement->text());

                    $marginVerseReferences = [];
                    $marginWordSynonyms = [];

                    $marginCrawler = new Crawler($marginHtml);
                    $marginCrawler->filterXPath('//a[@target=\'kanttekening_referentie\']')->each(function (Crawler $marginVerseReferenceCrawler) use (&$marginVerseReferences) {
                        $marginVerseReferences[] = $marginVerseReferenceCrawler->text();
                    });
                    foreach ($marginVerseReferences as $marginVerseReference) {
                        $marginText = str_replace($marginVerseReference, sprintf('<span class="ref">%s</span>', $marginVerseReference), $marginText);
                    }

                    $marginCrawler->filterXPath('//font[@color=\'green\']')->each(function (Crawler $marginWordSynonymCrawler) use (&$marginWordSynonyms) {
                        $marginWordSynonyms[] = $marginWordSynonymCrawler->text();
                    });
                    foreach ($marginWordSynonyms as $marginWordSynonym) {
                        $marginText = str_replace($marginWordSynonym, sprintf('<span class="m-word-synonym">%s</span>', $marginWordSynonym), $marginText);
                    }

                    $margin[$currentMarginNumber] = [
                        'number' => $currentMarginNumber,
                        'word' => $currentMarginWord,
                        'text' => $marginText,
                    ];

                    return;
                }
            });
        });

        return $margin;
    }

    private function getFlatVerses(array $chapterInArray): array
    {
        return array_map(function ($verseText) {
            return preg_replace(
                '/\s\s+/',
                ' ',
                strip_tags(
                    preg_replace(
                        '#<span class="m-number">(.*?)</span>#',
                        ' ',
                        $verseText
                    )
                )
            );
        }, $chapterInArray);
    }

    private function getFullChapter(array $chapterInArray): string
    {
        $chapter = '';

        foreach ($chapterInArray as $verseNumber => $verseText) {
            $chapter .= sprintf('<span class="n">%s</span>%s', $verseNumber, $verseText);
        }

        return $chapter;
    }

    private function getKeyWords(array $flatVerses): array
    {
        $keywords = [];

        foreach ($flatVerses as $verseNumber => $verseText) {
            $keywords[$verseNumber] = RakePlus::create($verseText, $this->stopWordsPattern)->keywords();
        }

        return $keywords;
    }
}
