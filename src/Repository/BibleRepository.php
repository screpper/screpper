<?php

namespace Screpper\Repository;

use Screpper\Entity\Bible;
use Screpper\Entity\Bible\Book;
use Screpper\Entity\Bible\Translation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Bible|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bible|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bible[]    findAll()
 * @method Bible[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BibleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bible::class);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function persist(Bible $bible)
    {
        $this->getEntityManager()->persist($bible);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    public function removeByTranslationBookAndChapter(Translation $translation, Book $book, int $chapter)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->delete('Screpper\Entity\Bible', 'b')
            ->where('b.translation = :translation')
            ->andWhere('b.book = :book')
            ->andWhere('b.chapter = :chapter')
            ->setParameters([
                'translation' => $translation,
                'book' => $book,
                'chapter' => $chapter,
            ])
            ->getQuery();

        $query->execute();
    }
}
