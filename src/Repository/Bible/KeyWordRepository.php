<?php

namespace Screpper\Repository\Bible;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Screpper\Entity\Bible\KeyWord;
use Screpper\Entity\Bible\Translation;

/**
 * @method KeyWord|null find($id, $lockMode = null, $lockVersion = null)
 * @method KeyWord|null findOneBy(array $criteria, array $orderBy = null)
 * @method KeyWord[]    findAll()
 * @method KeyWord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KeyWordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KeyWord::class);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function findRaw(Translation $translation, string $keyWord)
    {
        $connection = $this->getEntityManager()->getConnection();

        $sql = "SELECT * FROM bible_key_words WHERE bible_translation_id = :translation AND key_word = :keyWord";
        $statement = $connection->prepare($sql);
        $statement->bindValue('translation', $translation->getId());
        $statement->bindValue('keyWord', $keyWord);
        $statement->execute();

        $result = $statement->fetch();

        return false !== $result ? $result : null;
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function insert(Translation $translation, string $keyWord, array $searchStrings)
    {
        $connection = $this->getEntityManager()->getConnection();

        $connection->insert('bible_key_words', [
            'bible_translation_id' => $translation->getId(),
            'key_word' => $keyWord,
            'search_strings' => json_encode($searchStrings),
        ]);
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function update(string $id, array $searchStrings)
    {
        $connection = $this->getEntityManager()->getConnection();

        $connection->update('bible_key_words', [
            'search_strings' => json_encode($searchStrings)
        ], ['id' => $id]);
    }
}
