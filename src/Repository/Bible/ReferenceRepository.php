<?php

namespace Screpper\Repository\Bible;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Screpper\Entity\Bible;
use Screpper\Entity\Bible\Reference;

/**
 * @method Reference|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reference|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reference[]    findAll()
 * @method Reference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reference::class);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function persist(Reference $reference)
    {
        $this->getEntityManager()->persist($reference);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    public function removeByChapter(Bible $bible)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->delete('Screpper\Entity\Bible\Reference', 'br')
            ->where('br.translation = :translation')
            ->andWhere('br.book = :book')
            ->andWhere('br.chapter = :chapter')
            ->setParameter('translation', $bible->getTranslation()->getId())
            ->setParameter('book', $bible->getBook()->getId())
            ->setParameter('chapter', $bible->getChapter())
            ->getQuery();

        $query->execute();
    }
}