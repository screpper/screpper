<?php

declare(strict_types=1);

namespace Screpper\Repository\Bible;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Screpper\Entity\Bible\Book;
use Screpper\Entity\Bible\Marginal;
use Screpper\Entity\Bible\Reference;
use Screpper\Entity\Bible\Translation;

/**
 * @method Reference|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reference|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reference[]    findAll()
 * @method Reference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarginalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reference::class);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function persist(Marginal $reference)
    {
        $this->getEntityManager()->persist($reference);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function flush()
    {
        $this->getEntityManager()->flush();
    }

    public function removeByTranslationBookAndChapter(Translation $translation, Book $book, int $chapter)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->delete('Screpper\Entity\Bible\Marginal', 'bm')
            ->where('bm.translation = :translation')
            ->andWhere('bm.book = :book')
            ->andWhere('bm.chapter = :chapter')
            ->setParameters([
                'translation' => $translation,
                'book' => $book,
                'chapter' => $chapter,
            ])
            ->getQuery();

        $query->execute();
    }
}
