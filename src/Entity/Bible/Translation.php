<?php

namespace Screpper\Entity\Bible;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Screpper\Repository\Bible\TranslationRepository")
 * @ORM\Table(name="bible_translation")
 */
class Translation
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     *
     * @var string
     */
    private $name_short;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $copyright_nl;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $copyright_en;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     *
     * @var boolean
     */
    private $active;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNameShort()
    {
        return $this->name_short;
    }

    /**
     * @param string $name_short
     *
     * @return $this
     */
    public function setNameShort($name_short)
    {
        $this->name_short = $name_short;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCopyrightNl()
    {
        return $this->copyright_nl;
    }

    /**
     * @param string $copyrightNl
     *
     * @return $this
     */
    public function setCopyrightNl($copyrightNl)
    {
        $this->copyright_nl = $copyrightNl;

        return $this;
    }

    /**
     * @return string
     */
    public function getCopyrightEn()
    {
        return $this->copyright_en;
    }

    /**
     * @param string $copyrightEn
     *
     * @return $this
     */
    public function setCopyrightEn($copyrightEn)
    {
        $this->copyright_en = $copyrightEn;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }
}
