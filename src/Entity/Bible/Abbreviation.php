<?php

namespace Screpper\Entity\Bible;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Screpper\Repository\Bible\AbbreviationRepository")
 * @ORM\Table(name="bible_abbreviation")
 */
class Abbreviation
{
    /**
     * @ORM\Column(type="string", length=30)
     * @ORM\Id
     *
     * @var string
     */
    private $abbreviation;

    /**
     * @ORM\ManyToOne(targetEntity="Screpper\Entity\Bible\Book")
     * @ORM\JoinColumn(name="bible_book_id", referencedColumnName="id")
     *
     * @var \Screpper\Entity\Bible\Book
     */
    private $book;

    /**
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * @param string $abbreviation
     *
     * @return $this
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * @return \Screpper\Entity\Bible\Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param \Screpper\Entity\Bible\Book $book
     *
     * @return $this
     */
    public function setBook(Book $book)
    {
        $this->book = $book;

        return $this;
    }
}
