<?php

declare(strict_types=1);

namespace Screpper\Entity\Bible;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Screpper\Repository\Bible\KeyWordRepository")
 * @ORM\Table(
 *     name="bible_key_words",
 *     indexes={@ORM\Index(name="i_keyword", columns={"bible_translation_id", "key_word"})}
 * )
 */
class KeyWord
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Screpper\Entity\Bible\Translation")
     * @ORM\JoinColumn(name="bible_translation_id", referencedColumnName="id", nullable=true)
     *
     * @var \Screpper\Entity\Bible\Translation|null
     */
    private $translation;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @var string
     */
    private $keyWord;

    /**
     * @ORM\Column(type="json")
     *
     * @var array
     */
    private $searchStrings;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): KeyWord
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \Screpper\Entity\Bible\Translation|null
     */
    public function getTranslation(): ?Translation
    {
        return $this->translation;
    }

    /**
     * @param \Screpper\Entity\Bible\Translation|null $translation
     *
     * @return $this
     */
    public function setTranslation(?Translation $translation): KeyWord
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * @return string
     */
    public function getKeyWord(): string
    {
        return $this->keyWord;
    }

    /**
     * @param string $keyWord
     *
     * @return $this
     */
    public function setKeyWord(string $keyWord): KeyWord
    {
        $this->keyWord = $keyWord;

        return $this;
    }

    /**
     * @return array
     */
    public function getSearchStrings(): array
    {
        return $this->searchStrings;
    }

    /**
     * @param array $searchStrings
     *
     * @return $this
     */
    public function setSearchStrings(array $searchStrings): KeyWord
    {
        $this->searchStrings = $searchStrings;

        return $this;
    }
}
