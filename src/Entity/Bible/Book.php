<?php

namespace Screpper\Entity\Bible;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Screpper\Repository\Bible\BookRepository")
 * @ORM\Table(name="bible_book", uniqueConstraints={@ORM\UniqueConstraint(name="bible_book_name", columns={"name"})})
 */
class Book
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     *
     * @var string
     */
    private $testament;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $chapters;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTestament()
    {
        return $this->testament;
    }

    /**
     * @param string $testament
     *
     * @return $this
     */
    public function setTestament($testament)
    {
        $this->testament = $testament;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getChapters()
    {
        return $this->chapters;
    }

    /**
     * @param int $chapters
     *
     * @return $this
     */
    public function setChapters($chapters)
    {
        $this->chapters = $chapters;

        return $this;
    }
}
