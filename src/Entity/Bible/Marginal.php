<?php

declare(strict_types=1);

namespace Screpper\Entity\Bible;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Screpper\Repository\Bible\MarginalRepository")
 * @ORM\Table(
 *     name="bible_marginal",
 *     indexes={@ORM\Index(name="i_chapter", columns={"bible_translation_id", "bible_book_id", "chapter"})}
 * )
 */
class Marginal
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Screpper\Entity\Bible\Translation")
     * @ORM\JoinColumn(name="bible_translation_id", referencedColumnName="id")
     *
     * @var \Screpper\Entity\Bible\Translation
     */
    private $translation;

    /**
     * @ORM\ManyToOne(targetEntity="Screpper\Entity\Bible\Book")
     * @ORM\JoinColumn(name="bible_book_id", referencedColumnName="id")
     *
     * @var \Screpper\Entity\Bible\Book
     */
    private $book;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $chapter;

    /**
     * @ORM\Column(type="json")
     *
     * @var array
     */
    private $margins;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Marginal
    {
        $this->id = $id;

        return $this;
    }

    public function getTranslation(): Translation
    {
        return $this->translation;
    }

    public function setTranslation(Translation $translation): Marginal
    {
        $this->translation = $translation;

        return $this;
    }

    public function getBook(): Book
    {
        return $this->book;
    }

    public function setBook(Book $book): Marginal
    {
        $this->book = $book;

        return $this;
    }

    public function getChapter(): int
    {
        return $this->chapter;
    }

    public function setChapter(int $chapter): Marginal
    {
        $this->chapter = $chapter;

        return $this;
    }

    public function getMargins(): array
    {
        return $this->margins;
    }

    public function setMargins(array $margins): Marginal
    {
        $this->margins = $margins;

        return $this;
    }
}
