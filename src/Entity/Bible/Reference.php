<?php

namespace Screpper\Entity\Bible;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Screpper\Repository\Bible\ReferenceRepository")
 * @ORM\Table(
 *     name="bible_references",
 *     indexes={@ORM\Index(name="i_verse", columns={"bible_book_id", "chapter", "verse", "bible_translation_id"})}
 * )
 */
class Reference
{
    const TYPE_OFFICIAL = 'official';

    /** @var array */
    private $allowedTypes = [
        self::TYPE_OFFICIAL,
    ];

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Screpper\Entity\Bible\Translation")
     * @ORM\JoinColumn(name="bible_translation_id", referencedColumnName="id", nullable=true)
     *
     * @var \Screpper\Entity\Bible\Translation|null
     */
    private $translation;

    /**
     * @ORM\ManyToOne(targetEntity="Screpper\Entity\Bible\Book")
     * @ORM\JoinColumn(name="bible_book_id", referencedColumnName="id")
     *
     * @var \Screpper\Entity\Bible\Book
     */
    private $book;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $chapter;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $verse;

    /**
     * @ORM\Column(type="string", length=50)
     *
     * @var string
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=10)
     *
     * @var string
     */
    private $type = self::TYPE_OFFICIAL;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @var int
     */
    private $votes;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \Screpper\Entity\Bible\Translation|null
     */
    public function getTranslation(): ?Translation
    {
        return $this->translation;
    }

    /**
     * @param \Screpper\Entity\Bible\Translation|null $translation
     *
     * @return $this
     */
    public function setTranslation(?Translation $translation): Reference
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * @return \Screpper\Entity\Bible\Book
     */
    public function getBook(): Book
    {
        return $this->book;
    }

    /**
     * @param \Screpper\Entity\Bible\Book $book
     *
     * @return $this
     */
    public function setBook(Book $book): Reference
    {
        $this->book = $book;

        return $this;
    }

    /**
     * @return int
     */
    public function getChapter(): int
    {
        return $this->chapter;
    }

    /**
     * @param int $chapter
     *
     * @return $this
     */
    public function setChapter(int $chapter): Reference
    {
        $this->chapter = $chapter;

        return $this;
    }

    /**
     * @return int
     */
    public function getVerse(): int
    {
        return $this->verse;
    }

    /**
     * @param int $verse
     *
     * @return $this
     */
    public function setVerse(int $verse): Reference
    {
        $this->verse = $verse;

        return $this;
    }

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     *
     * @return $this
     */
    public function setReference(string $reference): Reference
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): Reference
    {
        if (!in_array($type, $this->allowedTypes)) {
            throw new \UnexpectedValueException(sprintf('Invalid type %s as bible verse reference.', $type));
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getVotes(): int
    {
        return $this->votes;
    }

    /**
     * @param int $votes
     *
     * @return $this
     */
    public function setVotes(int $votes): Reference
    {
        $this->votes = $votes;

        return $this;
    }
}
