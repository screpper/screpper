<?php

namespace Screpper\Entity;

use Screpper\Entity\Bible\Book;
use Screpper\Entity\Bible\Translation;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Screpper\Repository\BibleRepository")
 * @ORM\Table(name="bible", indexes={@ORM\Index(name="i_chapter", columns={"bible_translation_id", "bible_book_id", "chapter"})})
 */
class Bible
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Screpper\Entity\Bible\Translation")
     * @ORM\JoinColumn(name="bible_translation_id", referencedColumnName="id")
     *
     * @var \Screpper\Entity\Bible\Translation
     */
    private $translation;

    /**
     * @ORM\ManyToOne(targetEntity="Screpper\Entity\Bible\Book")
     * @ORM\JoinColumn(name="bible_book_id", referencedColumnName="id")
     *
     * @var \Screpper\Entity\Bible\Book
     */
    private $book;

    /**
     * @ORM\Column(type="smallint")
     *
     * @var int
     */
    private $chapter;

    /**
     * @ORM\Column(type="json")
     *
     * @var array
     */
    private $text;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Translation
     */
    public function getTranslation()
    {
        return $this->translation;
    }

    /**
     * @param \Screpper\Entity\Bible\Translation $translation
     *
     * @return $this
     */
    public function setTranslation(Translation $translation)
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * @return \Screpper\Entity\Bible\Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param \Screpper\Entity\Bible\Book $book
     *
     * @return $this
     */
    public function setBook(Book $book)
    {
        $this->book = $book;

        return $this;
    }

    /**
     * @return int
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * @param int $chapter
     *
     * @return $this
     */
    public function setChapter($chapter)
    {
        $this->chapter = $chapter;

        return $this;
    }

    /**
     * @return array
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param array $text
     *
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }
}
