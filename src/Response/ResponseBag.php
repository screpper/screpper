<?php

declare(strict_types=1);

namespace Screpper\Response;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolation;

class ResponseBag implements \JsonSerializable
{
    /** @var null|array */
    private $data = null;

    /** @var array */
    private $errors = [];

    /**
     * @return $this
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return $this
     */
    public function addFormErrors(FormInterface $form): self
    {
        $errors = $form->getErrors(true);
        if (0 === $errors->count()) {
            return $this;
        }

        $messages = [];
        foreach ($errors as $formError) {
            $cause = $formError->getCause();
            if (!$cause instanceof ConstraintViolation) {
                $messages[] = sprintf('Error in request: %s', $formError->getMessage());
            }

            $propertyPath = preg_replace('/^data\./', '', $cause->getPropertyPath());
            $messages[$propertyPath] = $formError->getMessage();
        }

        $this->errors = array_merge($this->errors, $messages);

        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'data' => $this->data,
            'meta' => $this->buildMeta(),
        ];
    }

    private function buildMeta(): array
    {
        $meta = [
            'count' => (!empty($this->data)) ? count($this->data) : 0,
            'status' => (0 === count($this->errors)) ? 'success' : 'error',
        ];

        if (0 !== count($this->errors)) {
            $meta['errors'] = $this->errors;
        }

        return $meta;
    }
}
