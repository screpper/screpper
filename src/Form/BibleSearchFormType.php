<?php

declare(strict_types=1);

namespace Screpper\Form;

use Screpper\Entity\Bible\Translation;
use Screpper\Model\BibleSearch;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BibleSearchFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('search', TextType::class, [
                'property_path' => 'searchString',
            ])
            ->add('translations', CollectionType::class, [
                'entry_type' => EntityType::class,
                'entry_options' => [
                    'class' => Translation::class,
                ],
                'allow_add' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BibleSearch::class,
        ]);
    }
}
